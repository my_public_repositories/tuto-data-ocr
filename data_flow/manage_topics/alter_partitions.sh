#!/bin/bash
TOPIC="${1:-purchases}"

docker compose exec broker \
  kafka-topics \
    --alter \
    --bootstrap-server localhost:9092 \
    --topic $TOPIC \
    --partitions 2
    # si on veut plusieurs consumer il faut augmenter le nombre de partition avec en console du kafka :
    # $ ./bin/kafka-.sh --alter --zookeeper localhost:2181 --topic blabla --partitions 2
