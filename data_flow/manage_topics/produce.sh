#!/bin/bash
TOPIC="${1:-purchases}"

docker compose exec broker \
  kafka-console-producer \
    --topic $TOPICases \
    --broker-list localhost:9092 \
    --timeout 0