#!/bin/bash
GROUP="${1:-mygroup}"

docker compose exec broker \
  kafka-consumer-groups \
    --bootstrap-server localhost:9092 \
    --describe --group  $GROUP