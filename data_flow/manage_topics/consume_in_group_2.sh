#!/bin/bash
TOPIC="${1:-purchases}"
GROUP="${1:-mygroup}"

docker compose exec broker \
  kafka-console-consumer \
    --topic $TOPIC \
    --bootstrap-server localhost:9092 \
    --consumer-property group.id=$GROUP #pour ne pas reprendre au début et ne pas rater de message en absence

