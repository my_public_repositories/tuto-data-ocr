#!/bin/bash
TOPIC="${1:-purchases}"
PARTITIONS="${2:-1}"
REPLICATION_FACTOR="${3:-1}"


docker compose exec broker \
  kafka-topics --create \
    --topic $TOPIC \
    --bootstrap-server localhost:9092 \
    --replication-factor $REPLICATION_FACTOR \
    --partitions $PARTITIONS