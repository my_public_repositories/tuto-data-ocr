#!/bin/bash
TOPIC="${1:-purchases}"

docker compose exec broker \
  kafka-console-consumer \
    --topic $TOPIC \
    --bootstrap-server localhost:9092 \
    --from-beginning # --consumer-property group.id=mygroup #pour ne pas reprendre au début et ne pas rater de message en absence

