#!/bin/bash
TOPIC="${1:-purchases}"

docker compose exec broker \
  kafka-topics --describe \
    --bootstrap-server localhost:9092 \
    --topic $TOPIC 